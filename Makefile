PRODUCT = id3
VERSION = 1.1.2

SHELL = /bin/sh

STD_CPPFLAGS ?= -D_POSIX_C_SOURCE=200809L -D_XOPEN_SOURCE=700

LFS_CPPFLAGS ?= `getconf LFS_CFLAGS || echo "-D_FILE_OFFSET_BITS=64"`
LFS_LDFLAGS ?= `getconf LFS_LDFLAGS || echo ""`

STD_CFLAGS ?= -std=c99
WARN_CFLAGS ?= -Wall -Wextra -Wbad-function-cast \
	-Wcast-align -Wcast-qual -Wchar-subscripts -Winline \
	-Wmissing-prototypes -Wnested-externs -Wpointer-arith \
	-Wredundant-decls -Wshadow -Wstrict-prototypes -Wwrite-strings

CC ?= gcc
CPPFLAGS ?=
CPPFLAGS += ${STD_CPPFLAGS} ${LFS_CPPFLAGS}
CFLAGS ?= -g -O2
CFLAGS += ${STD_CFLAGS} ${WARN_CFLAGS}
LDFLAGS ?=
LDFLAGS += ${LFS_LDFLAGS}
LIBS = 
DEFS = -DID3_VERSION='"${VERSION}"'
INSTALL ?= /usr/bin/install -c
STRIP ?= -s

# Installation directories
prefix = ${DESTDIR}/usr
exec_prefix = ${prefix}
mandir = ${prefix}/share/man/man1
bindir = ${exec_prefix}/bin

INCL = 
SRCS = id3.c
OBJS = $(SRCS:.c=.o)

.SUFFIXES: .c .o

.c.o:
	$(CC) $(CPPFLAGS) $(DEFS) $(CFLAGS) -c $<

all: $(PRODUCT)

$(PRODUCT): $(OBJS)
	$(CC) $(LDFLAGS) -o $@ $(OBJS) $(LIBS)

test: $(PRODUCT)
	env TEST_ID3='./$(PRODUCT)' prove t

clean:
	rm -f *~ *.o core $(PRODUCT)

install: $(PRODUCT)
	$(INSTALL) -d -m 755 $(bindir)
	$(INSTALL) $(STRIP) -m 755 $(PRODUCT) $(bindir)
	$(INSTALL) -d -m 755 $(mandir)
	$(INSTALL) -m 644 $(PRODUCT).1 $(mandir)
