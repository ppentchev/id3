#!/usr/bin/perl
#
# Copyright (c) 2020  Peter Pentchev <roam@ringlet.net>
# This code is hereby licensed for public consumption under either the
# GNU GPL v2 or greater, or Larry Wall's Artistic license - your choice.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

use v5.12;

use strict;
use warnings;

use File::Temp;
use Path::Tiny;
use Test::Command;
use Test::More;

plan tests => 7;

my $contents = ('A' x 128).('B' x 128);
my $contents_len = length $contents;

sub verify_contents($ $ $)
{
	my ($tag, $fdata, $expected) = @_;

	my $data = $fdata->slurp_utf8;
	my $exp_len = $contents_len + ($expected ? 128 : 0);

	subtest "verify data file: $tag" => sub {
		plan tests => 3;

		is length $data, $exp_len,
		    "The file is $exp_len characters long";
		is substr($data, 0, $contents_len), $contents,
		    'The file starts with the expected contents';

		SKIP: {
			skip 'No tag yet', 1 unless $expected;
			is substr($data, $contents_len, 3), 'TAG',
			    'The file contains the ID3 tag marker';
		}
	};
}

my $RE_TAG = qr{^
	(?<tag> [A-Za-z]+ )
	: \s
	(?<value_full>
		(?<ws_before> \s* )
		(?<value> .*? )
		(?<ws_after> \s* )
	)
$}x;

sub extract_file_tag_stripped($)
{
	my ($lines) = @_;

	my %data;
	subtest 'parse an RFC-822 tag' => sub {
		if (!@{$lines}) {
			fail 'any lines';
			return;
		}
		my $line = shift @{$lines};
		if ($line ne '') {
			unshift @{$lines}, $line;
			is $line, '', 'starts with an empty line';
			return;
		}

		while (@{$lines}) {
			my $line = shift @{$lines};
			if ($line eq '') {
				unshift @{$lines}, $line;
				last;
			}
			if ($line !~ $RE_TAG) {
				like $line, $RE_TAG, 'tag: value line';
				return;
			}
			my ($tag, $value) = ($+{tag}, $+{value});
			ok 1, "got a $tag line";
			$data{$tag} = $value;
		}
		fail 'any tag/value lines' unless %data;
	};

	return \%data;
}

my $PROG = $ENV{TEST_ID3} // './id3';
BAIL_OUT "Not an executable file: $PROG" unless -f $PROG && -x $PROG;

my $tempd = File::Temp->newdir('id3test.XXXXXX', TMPDIR => 1);
my $fdata = path($tempd)->child('data.dat');
$fdata->spew_utf8($contents);

verify_contents 'initial', $fdata, undef;

subtest 'List a file with no tag' => sub {
	plan tests => 3;

	my $cmd = [$PROG, '-l', '--', $fdata];
	my $tcmd = Test::Command->new(cmd => $cmd);
	$tcmd->exit_is_num(0, '-l succeeded');

	my @lines = split /\r*\n/, $tcmd->stdout_value;
	is scalar @lines, 1, '-l output a single line';
	like $lines[0], qr{No ID3 tag}, '-l said No ID3 tag';
};

subtest 'List (RFC-822) a file with no tag' => sub {
	plan tests => 5;

	my $cmd = [$PROG, '-l', '-R', '--', $fdata];
	my $tcmd = Test::Command->new(cmd => $cmd);
	$tcmd->exit_is_num(0, '-l -R succeeded');

	my @lines = split /\r*\n/, $tcmd->stdout_value;
	ok @lines >= 2, '-l -R output at least two lines';

	my $tag = extract_file_tag_stripped \@lines;
	is_deeply $tag, {
		Filename => "$fdata",
		Title => '',
		Artist => '',
		Album => '',
		Year => '',
		Genre => 'Unknown (255)',
		Comment => '',
	}, 'no tag';
	is_deeply \@lines, [], 'no more lines';
};

my %tag_data = (
	Filename => "$fdata",
	Title => 'Tenser, said the Tensor',
	Artist => 'Duffy Wyg&',
	Album => 'The Demolished Man',
	Year => '2301',
	Track => '1',
	Genre => 'Dream (55)',
	Comment => 'Tension, apprehension, and dissension',
);

subtest 'Add a tag' => sub {
	plan tests => 5;

	my $cmd = [
		$PROG,
		'-a', $tag_data{Artist},
		'-A', $tag_data{Album},
		'-y', $tag_data{Year},
		'-T', $tag_data{Track},
		'-t', $tag_data{Title},
		'-g', '55',
		'-c', $tag_data{Comment},
		'-R', '--', $tag_data{Filename},
	];
	my $tcmd = Test::Command->new(cmd => $cmd);
	$tcmd->exit_is_num(0, 'add with -R succeeded');

	# Cut the comment down to size
	$tag_data{Comment} = substr($tag_data{Comment}, 0, 28);

	my @lines = split /\r*\n/, $tcmd->stdout_value;
	ok @lines >= 2, 'add with -R output at least two lines';

	my $tag = extract_file_tag_stripped \@lines;
	is_deeply $tag, \%tag_data, 'no tag';
	is_deeply \@lines, [], 'no more lines';
};

verify_contents 'added', $fdata, \%tag_data;

subtest 'List the added tag' => sub {
	plan tests => 1 + scalar keys %tag_data;

	my $cmd = [$PROG, '-l', '--', $fdata];
	my $tcmd = Test::Command->new(cmd => $cmd);
	$tcmd->exit_is_num(0, '-l succeeded');

	my $output = $tcmd->stdout_value;
	for my $key (keys %tag_data) {
		my $value = $tag_data{$key};
		my $re = qr{\Q$value\E};
		like $output, $re, "found '$value' for $key";
	}
};

subtest 'List (RFC-822) the added tag' => sub {
	plan tests => 5;

	my $cmd = [$PROG, '-l', '-R', '--', $fdata];
	my $tcmd = Test::Command->new(cmd => $cmd);
	$tcmd->exit_is_num(0, '-l -R succeeded');

	my @lines = split /\r*\n/, $tcmd->stdout_value;
	ok @lines >= 2, '-l -R output at least two lines';

	my $tag = extract_file_tag_stripped \@lines;
	is_deeply $tag, \%tag_data, 'read the added data';
	is_deeply \@lines, [], 'no more lines';
};
